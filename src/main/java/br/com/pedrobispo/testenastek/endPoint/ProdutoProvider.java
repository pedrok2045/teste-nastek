package br.com.pedrobispo.testenastek.endPoint;

import java.util.List;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.pedrobispo.testenastek.model.Produto;
import br.com.pedrobispo.testenastek.repository.ProdutoRepository;

@RestController
@CrossOrigin
public class ProdutoProvider {
    
    @Autowired
    private ProdutoRepository produtoRepository;

    private static final Gson gson = new Gson();

    @RequestMapping(value = "/produtos", method = RequestMethod.GET)
	public ResponseEntity<Object> listarProdutosPorNomeAsc(){

        List<Produto> produtos = produtoRepository.buscarProdutosOrdenadosPeloNomeAsc();
		return new ResponseEntity<>(gson.toJson(produtos), HttpStatus.OK);
    }

    @RequestMapping(value = "/produtos/listagem/{ordem}", method = RequestMethod.GET)
	public ResponseEntity<Object> listarProdutosPorNome(@PathVariable("ordem") String ordem){
        List<Produto> produtos;

        switch(ordem){
            case "desc":
            produtos = produtoRepository.buscarProdutosOrdenadosPeloNomeDesc();
            break;

            default:
            produtos = produtoRepository.buscarProdutosOrdenadosPeloNomeAsc();
            break;
        }

		return new ResponseEntity<>(gson.toJson(produtos), HttpStatus.OK);
    }

    @RequestMapping(value = "/produtos/listagem/categoria/{idCategoria}", method = RequestMethod.GET)
	public ResponseEntity<Object> listarProdutosPorNome(@PathVariable("idCategoria") Long idCategoria){

        List<Produto> produtos = produtoRepository.buscarProdutosEhListarPorCategoria(idCategoria);
		return new ResponseEntity<>(gson.toJson(produtos), HttpStatus.OK);
    }

    @RequestMapping(value = "/produto/{idProduto}", method = RequestMethod.GET)
	public ResponseEntity<Object> buscarProdutoPorId(@PathVariable("ordem") Long idProduto){

        Produto produto = produtoRepository.findById(idProduto).get();
		return new ResponseEntity<>(gson.toJson(produto), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/produto", method = RequestMethod.POST)
	public ResponseEntity<Object> salvarProduto(@RequestBody Produto produto){

		produto = produtoRepository.save(produto);
		return new ResponseEntity<>(gson.toJson(produto), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/produto", method = RequestMethod.PUT)
	public ResponseEntity<Object> editarProduto(@RequestBody Produto produto){

		produto = produtoRepository.save(produto);
		return new ResponseEntity<>(gson.toJson(produto), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/produto/{idProduto}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarProdutoPorId(@PathVariable("idProduto") Long idProduto){

        produtoRepository.deleteById(idProduto);
		return new ResponseEntity<>(HttpStatus.OK);
    }
}