package br.com.pedrobispo.testenastek.endPoint;

import java.util.List;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.pedrobispo.testenastek.model.Categoria;
import br.com.pedrobispo.testenastek.repository.CategoriaRepository;

@RestController
@CrossOrigin
public class CategoriaProvider {
    
    @Autowired
    private CategoriaRepository categoriaRepository;

    private static final Gson gson = new Gson();


    @RequestMapping(value = "/categorias", method = RequestMethod.GET)
	public ResponseEntity<Object> buscarTodasCategorias(){

        List<Categoria> categorias = categoriaRepository.findAll();
		return new ResponseEntity<>(gson.toJson(categorias), HttpStatus.OK);
    }

    @RequestMapping(value = "/categoria/{idCategoria}", method = RequestMethod.GET)
	public ResponseEntity<Object> buscarCategoria(@PathVariable("idCategoria") Long idCategoria){

        Categoria categoria = categoriaRepository.findById(idCategoria).get();
		return new ResponseEntity<>(gson.toJson(categoria), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/categoria", method = RequestMethod.POST)
	public ResponseEntity<Object> salvarCategoria(@RequestBody Categoria categoria){
		categoria = categoriaRepository.save(categoria);

		return new ResponseEntity<>(gson.toJson(categoria), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/categoria", method = RequestMethod.PUT)
	public ResponseEntity<Object> editarCategoria(@RequestBody Categoria categoria){

		categoria = categoriaRepository.save(categoria);

		return new ResponseEntity<>(gson.toJson(categoria), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/categoria/{idCategoria}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> removerCategoria(@PathVariable("idCategoria") Long idCategoria){

        categoriaRepository.deleteById(idCategoria);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}