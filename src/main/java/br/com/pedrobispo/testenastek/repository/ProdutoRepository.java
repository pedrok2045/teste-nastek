package br.com.pedrobispo.testenastek.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import br.com.pedrobispo.testenastek.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {
    
    @Query("SELECT produto FROM Produto produto ORDER BY produto.nome")
    List<Produto> buscarProdutosOrdenadosPeloNomeAsc();

    @Query("SELECT produto FROM Produto produto ORDER BY produto.nome DESC")
    List<Produto> buscarProdutosOrdenadosPeloNomeDesc();

    //@Query("SELECT produto, COUNT(produt.nome) AS quantidade FROM Produto produto GROUP BY produto.nome, produto.id ORDER BY COUNT(produto.nome)")
    //List<Produto> buscarProdutosPorQuantidadeAsc();

    //@Query("SELECT produto, COUNT(produt.nome) AS quantidade FROM Produto produto GROUP BY produto.nome, produto.id ORDER BY COUNT(produto.nome) DESC")
    //List<Produto> buscarProdutosPorQuantidadeDesc();    

    void deleteByNome(String nome);

    @Query("SELECT produto FROM Produto produto JOIN produto.categoria categoria WHERE categoria.id =:idCategoria")
    List<Produto> buscarProdutosEhListarPorCategoria(@Param("idCategoria") Long idCategoria);

}