package br.com.pedrobispo.testenastek.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.pedrobispo.testenastek.model.Categoria;
import br.com.pedrobispo.testenastek.model.Produto;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestProdutoRepository {
    
    /**
     * refatorar as Strings para constantes.
     */

    @Autowired
    public ProdutoRepository produtoRepository;

    @Autowired
    public CategoriaRepository categoriaRepository;

    private Produto criarProduto(){
        Categoria categoria = new Categoria();
        categoria.setTitulo("titulo");
        categoriaRepository.save(categoria);

        Produto produto = new Produto();
        produto.setNome("nome");
        produto.setDescricao("descricao");
        produto.setCategoria(categoriaRepository.findById(categoria.getId()).get());
        return produto;
    }

    @Test
    public void testeSalvarProduto(){
        Produto produto = criarProduto();
        produtoRepository.save(produto);

        assertThat(produto.getId()).isNotNull();
    }

    @Test
    public void testeBuscarProdutoPorId(){
        Produto produto = criarProduto();
        produtoRepository.save(produto);
        assertThat(categoriaRepository.findById(produto.getId())).isNotNull();
    }

    @Test
    public void testeBuscarTodosOsProdutos(){
        int total_registros = 3;

        for(int i = 0 ; i < total_registros ; i++){
            Produto produto = criarProduto();
            produtoRepository.save(produto);
        }

        assertThat(produtoRepository.findAll().size()).isEqualTo(total_registros);
    }

    @Test
    public void TesteRemoverProdutoPorId(){
        Produto produto = criarProduto();
        produtoRepository.save(produto);
        
        produtoRepository.deleteById(produto.getId());
        assertThat(produtoRepository.findById(produto.getId()).isPresent()).isFalse();
    }

    @Test
    @Transactional
    public void testeRemoverProdutoPorNome(){
        Produto produto = criarProduto();
        produto.setNome("nome_do_produto");
        produtoRepository.save(produto);
        
        produtoRepository.deleteByNome(produto.getNome());
        assertThat(produtoRepository.findById(produto.getId()).isPresent()).isFalse();
    }

    @Test
    public void testeBuscarProdutosOrdenadosPeloNomeAsc(){
        Produto produto1 = criarProduto();
        produto1.setNome("caio");
        produtoRepository.save(produto1);

        Produto produto2 = criarProduto();
        produto2.setNome("amanda");
        produtoRepository.save(produto2);

        Produto produto3 = criarProduto();
        produto3.setNome("viviane");
        produtoRepository.save(produto3);

        assertThat(produtoRepository.buscarProdutosOrdenadosPeloNomeAsc().get(0).getNome()).isEqualTo("amanda");
        assertThat(produtoRepository.buscarProdutosOrdenadosPeloNomeAsc().get(1).getNome()).isEqualTo("caio");
        assertThat(produtoRepository.buscarProdutosOrdenadosPeloNomeAsc().get(2).getNome()).isEqualTo("viviane");
    }

    @Test
    public void testeBuscarProdutosEhListarPorCategoria(){
        Categoria categoria1 = new Categoria();
        categoriaRepository.save(categoria1);

        Categoria categoria2 = new Categoria();
        categoriaRepository.save(categoria2);

        Produto produto1 = criarProduto();
        produto1.setCategoria(categoria1);;
        produtoRepository.save(produto1);

        Produto produto2 = criarProduto();
        produto2.setCategoria(categoria1);;
        produtoRepository.save(produto2);

        Produto produto3 = criarProduto();
        produto3.setCategoria(categoria2);;
        produtoRepository.save(produto3);

        assertThat(produtoRepository.buscarProdutosEhListarPorCategoria(categoria1.getId()).size()).isEqualTo(2);
        assertThat(produtoRepository.buscarProdutosEhListarPorCategoria(categoria2.getId()).size()).isEqualTo(1);
    }


}