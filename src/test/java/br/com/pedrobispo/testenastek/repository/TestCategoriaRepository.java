package br.com.pedrobispo.testenastek.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.pedrobispo.testenastek.model.Categoria;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestCategoriaRepository {
    
    @Autowired
    public ProdutoRepository produtoRepository;

    @Autowired
    public CategoriaRepository categoriaRepository;

    private Categoria criarCategoria(){
        Categoria categoria = new Categoria();
        categoria.setTitulo("titulo");
        return categoria;
    }

    @Test
    public void salvarCategoria(){
        Categoria categoria = criarCategoria();
        categoriaRepository.save(categoria);
        assertThat(categoria.getId()).isNotNull();
    }

    @Test
    public void buscarCategoriaPorId(){
        Categoria categoria = criarCategoria();
        categoriaRepository.save(categoria);
        assertThat(categoriaRepository.findById(categoria.getId())).isNotNull();
    }

    @Test
    public void buscarTodasAsCategorias(){
        int total_registros = 3;

        for(int i = 0 ; i < total_registros ; i++){
            Categoria categoria = criarCategoria();
            categoriaRepository.save(categoria);
        }

        assertThat(categoriaRepository.findAll().size()).isEqualTo(total_registros);
    }

    @Test
    public void removerCategoria(){
        Categoria categoria = criarCategoria();
        categoriaRepository.save(categoria);
        categoriaRepository.delete(categoriaRepository.findById(categoria.getId()).get());
        assertThat(categoriaRepository.findById(categoria.getId()).isPresent()).isFalse();
    }
}